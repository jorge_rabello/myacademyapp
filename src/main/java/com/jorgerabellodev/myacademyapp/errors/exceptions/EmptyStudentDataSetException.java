package com.jorgerabellodev.myacademyapp.errors.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class EmptyStudentDataSetException extends RuntimeException {

  public EmptyStudentDataSetException(String message) {
    super(message);
  }

}
