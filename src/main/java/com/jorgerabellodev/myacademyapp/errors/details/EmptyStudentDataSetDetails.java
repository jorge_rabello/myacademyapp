package com.jorgerabellodev.myacademyapp.errors.details;

public class EmptyStudentDataSetDetails extends ErrorDetail {

  public static final class Builder {

    private String title;
    private Integer status;
    private String detail;
    private Long timestamp;
    private String developerMessage;

    private Builder() {
    }

    public static Builder newBuilder() {
      return new Builder();
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder status(Integer status) {
      this.status = status;
      return this;
    }

    public Builder detail(String detail) {
      this.detail = detail;
      return this;
    }

    public Builder timestamp(Long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    public Builder developerMessage(String developerMessage) {
      this.developerMessage = developerMessage;
      return this;
    }

    public EmptyStudentDataSetDetails build() {
      EmptyStudentDataSetDetails emptyStudentDataSetDetails = new EmptyStudentDataSetDetails();
      emptyStudentDataSetDetails.setDeveloperMessage(this.developerMessage);
      emptyStudentDataSetDetails.setTitle(this.title);
      emptyStudentDataSetDetails.setTimestamp(this.timestamp);
      emptyStudentDataSetDetails.setDetail(this.detail);
      emptyStudentDataSetDetails.setStatus(this.status);
      return emptyStudentDataSetDetails;
    }
  }
}
