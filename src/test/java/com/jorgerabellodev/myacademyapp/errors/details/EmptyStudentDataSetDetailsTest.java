package com.jorgerabellodev.myacademyapp.errors.details;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import com.jorgerabellodev.myacademyapp.errors.details.EmptyStudentDataSetDetails.Builder;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmptyStudentDataSetDetailsTest {

  private Long now;

  @BeforeEach
  public void beforeEach() {
    now = new Date().getTime();
  }

  @Test
  public void whenTryToCreateANewBuilderShouldReturnAnInstanceOfEmptyStudentDataSetDetailsBuilder() {

    Builder builder = Builder.newBuilder();
    assertNotNull(builder);
  }

  @Test
  public void whenTryToCreateANewAlreadyRegistredStudentDetailsShouldReturnAnInstanceOfEmptyStudentDataSetDetails() {

    EmptyStudentDataSetDetails emptyStudentDataSetDetails = new EmptyStudentDataSetDetails();
    emptyStudentDataSetDetails.setDeveloperMessage(any());
    emptyStudentDataSetDetails.setTitle(any());
    emptyStudentDataSetDetails.setTimestamp(now);
    emptyStudentDataSetDetails.setDetail(any());
    emptyStudentDataSetDetails.setStatus(any());

    assertNotNull(emptyStudentDataSetDetails);
    assertEquals(now, emptyStudentDataSetDetails.getTimestamp());
  }


}
