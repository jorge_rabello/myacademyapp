package com.jorgerabellodev.myacademyapp.services;

import com.jorgerabellodev.myacademyapp.errors.exceptions.AlreadyRegistredStudentException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.EmptyStudentDataSetException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.ResourceNotFoundException;
import com.jorgerabellodev.myacademyapp.models.dtos.request.NewStudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.request.StudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.response.StudentResponseDTO;
import com.jorgerabellodev.myacademyapp.models.entities.Student;
import com.jorgerabellodev.myacademyapp.repositories.StudentRepository;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService implements IStudentService {

  private final StudentRepository repository;
  private final ModelMapper modelMapper;

  @Autowired
  public StudentService(StudentRepository repository, ModelMapper modelMapper) {
    this.repository = repository;
    this.modelMapper = modelMapper;
  }

  @Override
  public StudentResponseDTO save(NewStudentRequestDTO newStudentRequestDTO) {
    Student student = modelMapper.map(newStudentRequestDTO, Student.class);
    Student registeredStudent = repository.findByEmail(student.getEmail());
    if (registeredStudent != null) {
      throw new AlreadyRegistredStudentException("Este estudante já está cadastrado");
    }
    Student savedStudent = repository.save(student);
    return modelMapper.map(savedStudent, StudentResponseDTO.class);
  }

  @Override
  public StudentResponseDTO findById(long id) {
    if (!repository.existsById(id)) {
      throw new ResourceNotFoundException("Estudante não encontrado");
    }
    Student student = repository.getOne(id);
    return modelMapper.map(student, StudentResponseDTO.class);
  }

  @Override
  public List<StudentResponseDTO> findAll() {
    List<Student> everybody = repository.findAll();
    List<StudentResponseDTO> students = new ArrayList<>();
    if (everybody.isEmpty()) {
      throw new EmptyStudentDataSetException("Não há estudantes cadastrados");
    }
    for (Student s : everybody) {
      StudentResponseDTO studentResponseDTO = modelMapper.map(s, StudentResponseDTO.class);
      students.add(studentResponseDTO);
    }
    return students;
  }

  @Override
  public void delete(Long id) {
    if (!repository.existsById(id)) {
      throw new ResourceNotFoundException("Estudante não encontrado");
    }
    repository.deleteById(id);
  }

  @Override
  public StudentResponseDTO update(StudentRequestDTO studentRequestDTO) {
    if (!repository.existsById(studentRequestDTO.getId())) {
      throw new ResourceNotFoundException("Estudante não encontrado");
    }
    Student student = modelMapper.map(studentRequestDTO, Student.class);
    Student savedStudent = repository.save(student);
    return modelMapper.map(savedStudent, StudentResponseDTO.class);
  }

}
