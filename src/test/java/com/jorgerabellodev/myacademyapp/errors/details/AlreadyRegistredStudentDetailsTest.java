package com.jorgerabellodev.myacademyapp.errors.details;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import com.jorgerabellodev.myacademyapp.errors.details.AlreadyRegistredStudentDetails.Builder;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

class AlreadyRegistredStudentDetailsTest {

  private Long now;

  @BeforeEach
  public void beforeEach() {
    now = new Date().getTime();
  }

  @Test
  public void whenTryToCreateANewBuilderShouldReturnAnInstanceOfAlreadyRegistredStudentDetailsBuilder() {

    Builder builder = Builder.newBuilder();
    assertNotNull(builder);
  }

  @Test
  public void whenTryToCreateANewAlreadyRegistredStudentDetailsShouldReturnAnInstanceOfAlreadyRegistredStudentDetails() {

    AlreadyRegistredStudentDetails alreadyRegistredStudentDetails = Builder
        .newBuilder()
        .timestamp(now)
        .status(HttpStatus.CONFLICT.value())
        .title("This student is already registered")
        .detail(any())
        .developerMessage(any())
        .build();

    assertNotNull(alreadyRegistredStudentDetails);
    assertEquals(now, alreadyRegistredStudentDetails.getTimestamp());
    assertEquals(409, alreadyRegistredStudentDetails.getStatus());
    assertEquals("This student is already registered", alreadyRegistredStudentDetails.getTitle());

  }

}
