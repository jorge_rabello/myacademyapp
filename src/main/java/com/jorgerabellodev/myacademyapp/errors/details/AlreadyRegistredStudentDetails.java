package com.jorgerabellodev.myacademyapp.errors.details;

public class AlreadyRegistredStudentDetails extends ErrorDetail {

  public static final class Builder {

    private String title;
    private Integer status;
    private String detail;
    private Long timestamp;
    private String developerMessage;

    private Builder() {
    }

    public static Builder newBuilder() {
      return new Builder();
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder status(Integer status) {
      this.status = status;
      return this;
    }

    public Builder detail(String detail) {
      this.detail = detail;
      return this;
    }

    public Builder timestamp(Long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    public Builder developerMessage(String developerMessage) {
      this.developerMessage = developerMessage;
      return this;
    }

    public AlreadyRegistredStudentDetails build() {
      AlreadyRegistredStudentDetails alreadyRegistredStudentDetails = new AlreadyRegistredStudentDetails();
      alreadyRegistredStudentDetails.setStatus(this.status);
      alreadyRegistredStudentDetails.setTimestamp(this.timestamp);
      alreadyRegistredStudentDetails.setDeveloperMessage(this.developerMessage);
      alreadyRegistredStudentDetails.setDetail(this.detail);
      alreadyRegistredStudentDetails.setTitle(this.title);
      return alreadyRegistredStudentDetails;
    }
  }
}
