package com.jorgerabellodev.myacademyapp.services;

import com.jorgerabellodev.myacademyapp.models.dtos.request.NewStudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.request.StudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.response.StudentResponseDTO;
import java.util.List;

public interface IStudentService {

  StudentResponseDTO save(NewStudentRequestDTO newStudentRequestDTO);

  StudentResponseDTO findById(long id);

  List<StudentResponseDTO> findAll();

  void delete(Long id);

  StudentResponseDTO update(StudentRequestDTO studentRequestDTO);
}
