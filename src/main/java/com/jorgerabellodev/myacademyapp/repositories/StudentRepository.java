package com.jorgerabellodev.myacademyapp.repositories;

import com.jorgerabellodev.myacademyapp.models.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

  Student findByEmail(String email);

}
