package com.jorgerabellodev.myacademyapp.controllers;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import com.jorgerabellodev.myacademyapp.controllers.api.StudentController;
import com.jorgerabellodev.myacademyapp.errors.exceptions.AlreadyRegistredStudentException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.EmptyStudentDataSetException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.ResourceNotFoundException;
import com.jorgerabellodev.myacademyapp.models.dtos.request.NewStudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.request.StudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.response.StudentResponseDTO;
import com.jorgerabellodev.myacademyapp.services.IStudentService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class StudentControllerTest {

  @Mock
  private IStudentService service;

  @InjectMocks
  private StudentController endpoint;

  @BeforeEach
  public void setup() {
    endpoint = new StudentController(service);
  }

  @Test
  public void givenAStudentPayloadWhenTryToSaveShouldReturnTheSavedStudentAndCreatedHTTPStatusCode() {

    NewStudentRequestDTO newStudentRequestDTO = new NewStudentRequestDTO("Jorge",
        "jorge@email.com");
    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge", "jorge@email.com");

    when(service.save(newStudentRequestDTO)).thenReturn(studentResponseDTO);

    ResponseEntity<?> response = endpoint
        .save(newStudentRequestDTO, MediaType.APPLICATION_JSON_VALUE);

    StudentResponseDTO savedStudentDTO = (StudentResponseDTO) response.getBody();

    assertNotNull(savedStudentDTO);
    assertEquals(1, savedStudentDTO.getId());
    assertEquals("Jorge", savedStudentDTO.getName());
    assertEquals("jorge@email.com", savedStudentDTO.getEmail());
    assertEquals(HttpStatus.CREATED, response.getStatusCode());

  }

  @Test
  public void givenAStudentIDAndTheIDExistsWhenTryToRetrieveASavedStudentShouldReturnTheSavedStudentAndOKHTTPStatusCode() {

    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge", "jorge@email.com");

    when(service.findById(1L)).thenReturn(studentResponseDTO);

    ResponseEntity<?> response = endpoint.findById(1L, MediaType.APPLICATION_JSON_VALUE);

    StudentResponseDTO savedStudent = (StudentResponseDTO) response.getBody();

    assertNotNull(savedStudent);
    assertEquals(1, savedStudent.getId());
    assertEquals("Jorge", savedStudent.getName());
    assertEquals("jorge@email.com", savedStudent.getEmail());
    assertEquals(HttpStatus.OK, response.getStatusCode());

  }

  @Test
  public void givenAStudentIDAndTheIDDoesNotExistsWhenTryToRetrieveASavedStudentShouldReturnAnNotFoundHTTPStatusCode() {

    when(service.findById(100L)).thenThrow(ResourceNotFoundException.class);
    assertThrows(ResourceNotFoundException.class,
        () -> endpoint.findById(100L, MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void whenTryToRetrieveAllSavedStudentsShouldReturnAllTheSavedStudentsAndOKHTTPStatusCode() {

    List<StudentResponseDTO> students = asList(
        new StudentResponseDTO(1L, "Jorge", "jorge@email.com"),
        new StudentResponseDTO(2L, "Maria", "maria@email.com"),
        new StudentResponseDTO(3L, "Eduarda", "eduarda@email.com"),
        new StudentResponseDTO(4L, "José", "jose@email.com"),
        new StudentResponseDTO(5L, "João", "joao@email.com")
    );

    when(service.findAll()).thenReturn(students);

    ResponseEntity<?> response = endpoint.findAll(MediaType.APPLICATION_JSON_VALUE);

    List<StudentResponseDTO> savedStudents = (List<StudentResponseDTO>) response.getBody();

    assertNotNull(savedStudents);
    assertFalse(savedStudents.isEmpty());
    assertEquals("Jorge", savedStudents.get(0).getName());
    assertEquals("jorge@email.com", savedStudents.get(0).getEmail());
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  public void whenTryToRetrieveAllSavedStudentsAndTheresAreNoSavedStudentsShouldReturnNoContentHTTPStatus() {

    when(service.findAll()).thenThrow(EmptyStudentDataSetException.class);
    assertThrows(EmptyStudentDataSetException.class,
        () -> endpoint.findAll(MediaType.APPLICATION_JSON_VALUE));

  }

  @Test
  public void givenAStudentIdWhenTryToDeleteASavedStudentShouldReturnNoContentHTTPStatus() {

    ResponseEntity<?> response = endpoint.delete(1L, MediaType.APPLICATION_JSON_VALUE);

    assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
  }

  @Test
  public void givenAStudentIdThatDoesNotExistsWhenTryToDeleteASavedStudentShouldReturnNotFoundHTTPStatus() {

    doThrow(ResourceNotFoundException.class).when(service).delete(100L);

    assertThrows(ResourceNotFoundException.class,
        () -> endpoint.delete(100L, MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void givenAStudentPayloadAndAStudentIDWhenTryToUpdateThisAttributesShouldReturnTheUpdatedStudentAndOKHTTPStatusCode() {

    StudentRequestDTO studentRequestDTO = new StudentRequestDTO(1L, "Jorge",
        "jorge.rabello@email.com");

    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge Rabello",
        "jorge.rabello@email.com");

    when(service.update(studentRequestDTO)).thenReturn(studentResponseDTO);

    ResponseEntity<?> response = endpoint
        .update(studentRequestDTO, MediaType.APPLICATION_JSON_VALUE);

    StudentResponseDTO updatedStudent = (StudentResponseDTO) response.getBody();

    assertNotNull(updatedStudent);
    assertEquals(1, updatedStudent.getId());
    assertEquals("Jorge Rabello", updatedStudent.getName());
    assertEquals("jorge.rabello@email.com", updatedStudent.getEmail());
    assertEquals(HttpStatus.OK, response.getStatusCode());

  }

  @Test
  public void givenAStudentIdThatDoesNotExistsWhenTryToUpdateASavedStudentShouldReturnNotFoundHTTPStatus() {

    StudentRequestDTO studentRequestDTO = new StudentRequestDTO(1L, "Jorge",
        "jorge.rabello@email.com");

    when(service.update(studentRequestDTO)).thenThrow(ResourceNotFoundException.class);

    assertThrows(ResourceNotFoundException.class,
        () -> endpoint.update(studentRequestDTO, MediaType.APPLICATION_JSON_VALUE));

  }

  @Test
  public void givenAStudentwhoseEmailIsAlreadyRegisteredWhenTryToSaveShouldThrowAlreadyRegistredStudentException() {

    NewStudentRequestDTO newStudentRequestDTO = new NewStudentRequestDTO("Jorge",
        "jorge@email.com");

    when(service.save(newStudentRequestDTO)).thenThrow(AlreadyRegistredStudentException.class);

    assertThrows(AlreadyRegistredStudentException.class,
        () -> endpoint.save(newStudentRequestDTO, MediaType.APPLICATION_JSON_VALUE));
  }

}
